function generateTable() {
  let placeholder = document.getElementsByClassName('container')[0];

  let table = document.createElement('table');
  let thead = document.createElement('thead');
  let tbody = document.createElement('tbody');

  table.setAttribute('class', 'table table-striped');

  // Add table head
  let rowsInHead = document.createElement('tr');
  for (let i = 0; i < 4; i++) {
    let head = document.createElement('th');
    head.setAttribute('scope', 'col');
    rowsInHead.appendChild(head);
  }
  rowsInHead.childNodes[0].innerHTML = "#";
  rowsInHead.childNodes[1].innerHTML = "First";
  rowsInHead.childNodes[2].innerHTML = "Last";
  rowsInHead.childNodes[3].innerHTML = "Handle";

  //Add table body
  for (let i = 0; i < 3; i++) {
    let bodyRow = document.createElement('tr');
    let head = document.createElement('th');
    head.setAttribute('scope', 'row');
    bodyRow.appendChild(head);
    for (let i = 0; i < 3; i++) {
      bodyRow.appendChild(document.createElement('td'));
    }
    tbody.appendChild(bodyRow);
  }
  tbody.childNodes[0].childNodes[0].innerHTML = "1";
  tbody.childNodes[0].childNodes[1].innerHTML = "Mark";
  tbody.childNodes[0].childNodes[2].innerHTML = "Otto";
  tbody.childNodes[0].childNodes[3].innerHTML = "@mdo";

  tbody.childNodes[1].childNodes[0].innerHTML = "2";
  tbody.childNodes[1].childNodes[1].innerHTML = "Jacob";
  tbody.childNodes[1].childNodes[2].innerHTML = "Thornton";
  tbody.childNodes[1].childNodes[3].innerHTML = "@fat";

  tbody.childNodes[2].childNodes[0].innerHTML = "3";
  tbody.childNodes[2].childNodes[1].innerHTML = "Larry";
  tbody.childNodes[2].childNodes[2].innerHTML = "the Bird";
  tbody.childNodes[2].childNodes[3].innerHTML = "@twitter";

  table.appendChild(thead);
  thead.appendChild(rowsInHead);
  table.appendChild(tbody);
  placeholder.appendChild(table);
}