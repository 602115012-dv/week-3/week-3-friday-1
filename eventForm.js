"use strict";

function showAlert(text) {
  console.log(text);
}

function listFormElement() {
  var formElements = document.getElementsByTagName('form');
  console.log(formElements);
}

function copyTextFromNameToLastName() {
  var nameElement = document.getElementById('firstName');
  var surnameElement = document.getElementById('lastName');
  var firstName = nameElement.value;
  console.log(`firstname is ${firstName}`);
  console.log(`now lastname is ${surnameElement.value}`);
  surnameElement.value = firstName;
}

function Reset() {
  let elements = document.getElementsByTagName('form');
  for (let i = 0; i < elements[0].length; i++) {
    if (elements[0][i].getAttribute('type') == 'radio') {
      elements[0][i].checked = false;
    } else {
      elements[0][i].value = null;
    }
  }

  hideSummary();
}

function copyRadioToEmail() {
  let radioElements = document.getElementById('studentStatus')
      .getElementsByTagName('input');
  for (let i=0; i < radioElements.length; i++) {
    if (radioElements[i].checked) {
      document.getElementById('email').value = 
      radioElements[i].value;
    }
  }
}

function showSummary() {
  let summaryElement = document.getElementById('summary');
  summaryElement.removeAttribute('hidden')

  if (summaryElement.innerHTML != "") {
    summaryElement.innerHTML = "";
  }


  
  let name = document.getElementById('firstName').value;
  summaryElement.innerHTML += `<b>First name</b>: ${name} <br />`;
  
  let lastName = document.getElementById('lastName').value;
  summaryElement.innerHTML += `<b>Last name</b>: ${lastName} <br />`;

  let email = document.getElementById('email').value;
  summaryElement.innerHTML += `<b>Email</b>: ${email} <br />`;

  let password = document.getElementById('password').value;
  summaryElement.innerHTML += `<b>Password</b>: ${password} <br />`;

  let studentStatus = getStatus();
  summaryElement.innerHTML += `<b>Student status</b>: ${studentStatus} <br />`;
}

function getStatus() {
  let radioElements = document.getElementById('studentStatus')
      .getElementsByTagName('input');
  for (let i=0; i < radioElements.length; i++) {
    if (radioElements[i].checked) {
      return radioElements[i].value;
    }
  }
}

function hideSummary() {
  let summaryElement = document.getElementById('summary');
  summaryElement.setAttribute('Hidden', 'true');
}

function isEmpty(id) {
  if (document.getElementById(id).value == "") {
    hideSummary();
  }
}

function addPElement() {
  console.log('Pass');
  var node = document.createElement('p');
  node.innerHTML = 'Hello world';
  var placeHolder = document.getElementById('placeholder');
  placeHolder.appendChild(node);
}

function addHref() {
  var node = document.createElement('a');
  node.innderHTML = 'google';
  node.setAttribute('href', 'https://www.google.com');
  node.setAttribute('target', '_blank');
  var placeHolder = document.getElementById('placeHodler');
  placeHolder.appendChild(node);
}

function addBoth() {
  var node = document.createElement('p');
  node.innerHTML = 'Hello world';
  var node2 = document.createElement('a');
  node2.innerHTML = 'google';
  node2.setAttribute('href', 'https://www.google.com');
  node2.setAttribute('target', '_blank');
  var placeHolder = document.getElementById('placeholder');
  placeHolder.appendChild(node);
  placeHolder.appendChild(node2);
}

function generateForm() {
  let placeHolder = document.getElementsByClassName('container')[0];
  let form = document.createElement('form');
  let row = document.createElement('div');
  let header = document.createElement('div');
  let headerText = document.createElement('p');
  let colSeperator = document.createElement('div');
  let nameCol = document.createElement('div');
  let lastNameCol = document.createElement('div');
  let emailCol = document.createElement('div');
  let formGroup = document.createElement('div');
  let formGroup2 = document.createElement('div');
  let formGroup3 = document.createElement('div');
  let nameInput = document.createElement('input');
  let lastNameInput = document.createElement('input');
  let emailInput = document.createElement('input');

  headerText.innerHTML = 'Please fill this form to create account';

  email.setAttribute('placeholder', 'Email');
  email.setAttribute('class', 'form-control');
  email.setAttribute('type', 'email');
  lastNameInput.setAttribute('placeholder', 'Last name');
  lastNameInput.setAttribute('class', 'form-control');
  lastNameInput.setAttribute('type', 'text');
  nameInput.setAttribute('placeholder', 'First name');
  nameInput.setAttribute('class', 'form-control');
  nameInput.setAttribute('type', 'text');
  formGroup.setAttribute('class', 'form-group');
  formGroup2.setAttribute('class', 'form-group');
  formGroup3.setAttribute('class', 'form-group');
  nameCol.setAttribute('class', 'col-6');
  lastNameCol.setAttribute('class', 'col-6');
  emailCol.setAttribute('class', 'col');
  colSeperator.setAttribute('class', 'w-100');
  headerText.setAttribute('class', 'lead');
  header.setAttribute('class', 'col');
  row.setAttribute('class', 'row');

  
  form.appendChild(row);
  row.appendChild(header);
  header.appendChild(headerText);
  row.appendChild(colSeperator);
  row.appendChild(nameCol);
  formGroup.appendChild(nameInput);
  nameCol.appendChild(formGroup);
  row.appendChild(lastNameCol);
  lastNameCol.appendChild(formGroup2);
  formGroup2.appendChild(lastNameInput);
  row.appendChild(emailCol);
  emailCol.appendChild(formGroup3);
  formGroup3.appendChild(email);

  placeHolder.appendChild(form);
}