function generateTableByNumber() {
  let placeholder = document.getElementsByClassName('container')[0];

  let table = document.createElement('table');
  let thead = document.createElement('thead');
  let tbody = document.createElement('tbody');

  let number = document.getElementById('number').value;

  table.setAttribute('class', 'table');

  let rowInHead = document.createElement('tr');
  let headInHeadRow = document.createElement('th');
  headInHeadRow.setAttribute('scope', 'col');
  headInHeadRow.innerHTML = "#";
  rowInHead.appendChild(headInHeadRow);
  thead.appendChild(rowInHead);

  if (number % 1 != 0) {
    return;
  }

  for (let i = 0; i < number; i++) {
    let bodyRow = document.createElement('tr');
    let rowData = document.createElement('td');
    rowData.innerHTML = i + 1;
    bodyRow.appendChild(rowData);
    tbody.appendChild(bodyRow);
  }

  table.appendChild(thead);
  table.appendChild(tbody);
  placeholder.appendChild(table);
}